/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller;
import DAO.DAO_resident;
import DAO.connection;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import model.resident_model;
import model.resident_table_model;
import view.resident_view;
import java.io.File;
import javax.swing.JOptionPane;

/**
 *
 * @author israj
 */
public class resident_controller {
    resident_view residentView;
    resident_model residentModel=new resident_model();
    DAO_resident DAOresident;
    List<resident_model> residentList;
    public resident_controller(resident_view residentView)
    {
        DAOresident=new DAO_resident();
        this.residentView=residentView;
    }
    public void New()
    {
        String id=residentView.getTxtID().getText();
        String name=residentView.getTxtName().getText();
        String phone=residentView.getTxtPhone().getText();
        String address=residentView.getTxtAddress().getText();
        residentModel.setId(id);
        residentModel.setName(name);
        residentModel.setPhone(phone);
        residentModel.setAddress(address);
        DAOresident.New(residentModel);
        Reset();
    }
    public void Update()
    {
        String id=residentView.getTxtID().getText();
        String name=residentView.getTxtName().getText();
        String phone=residentView.getTxtPhone().getText();
        String address=residentView.getTxtAddress().getText();
        residentModel.setId(id);
        residentModel.setName(name);
        residentModel.setPhone(phone);
        residentModel.setAddress(address);
        DAOresident.Update(residentModel);
        Reset();
    }
    public void Delete()
    {
        String id=residentView.getTxtID().getText();
        residentModel.setId(id);
        DAOresident.Delete(id);
        Reset();
    }
     public void Reset()
    {
        residentView.getTxtID().setText("");
        residentView.getTxtName().setText("");
        residentView.getTxtPhone().setText("");
        residentView.getTxtAddress().setText(""); 
        residentView.getTxtCari().setText("");
        LoadData();
    }
     public void getCari()
    {
        if (!residentView.getTxtCari().getText().trim().isEmpty()) {
            DAOresident.getCari(residentView.getTxtCari().getText());
            isiTableCari();
        } else {
            JOptionPane.showMessageDialog(residentView, "Please enter keywords and press button SEARCH");
        }
    }
     
     
     //menampilkan data ke dalam tabel
    public void LoadData() {
        residentList = DAOresident.getList();
        resident_table_model residentTable = new resident_table_model(residentList);
        residentView.getTabelData().setModel(residentTable);
    }

    //merupakan fungsi untuk menampilkan data yang dipilih dari tabel
    public void ChooseData(int row) {
        residentView.getTxtID().setText(residentList.get(row).getId());
        residentView.getTxtName().setText(residentList.get(row).getName());
        residentView.getTxtPhone().setText(residentList.get(row).getPhone());
        residentView.getTxtAddress().setText(residentList.get(row).getAddress());
    }
    
     public void isiTableCari() {
        residentList = DAOresident.getCari(residentView.getTxtCari().getText());
        resident_table_model residentTable = new resident_table_model(residentList);
        residentView.getTabelData().setModel(residentTable);
    }
     public void Report() {
         DAOresident.Report(residentModel);
    }
     
     
  
}
