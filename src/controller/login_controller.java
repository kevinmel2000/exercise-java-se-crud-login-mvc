/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controller;
import model.login_model;
import view.login_view;
import DAO.DAO_login;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import view.resident_view;
/**
 *
 * @author israj
 */
public class login_controller {
    login_model loginModel=new login_model();
    login_view loginView;
    DAO_login DAOlogin=new DAO_login();
    resident_view residentView=new resident_view();

    public login_controller(login_view loginView)
    {
    this.loginView=loginView;
    }
    public void validate()
    {
        String username=loginView.getTxtUsername().getText();
        String password=loginView.getTxtPassword().getText();
        loginModel.setUsername(username);
        loginModel.setPassword(password);
        if (DAOlogin.isValidUser(loginModel))
        {
            JOptionPane.showMessageDialog(null, "welcome");
            residentView.setVisible(true);
            loginView.dispose();
        }
        else
        {
            JOptionPane.showMessageDialog(null, "Not Authorized User");
        }
    }
}
