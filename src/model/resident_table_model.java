/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.util.List;
import javax.swing.table.AbstractTableModel;
/**
 *
 * @author israj
 */
public class resident_table_model extends AbstractTableModel {
    
     List<resident_model> residentList;

    public resident_table_model(List<resident_model> residentList) {
        this.residentList = residentList;
    }
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "ID";
            case 1:
                return "Name";
            case 2:
                return "Phone";
            case 3:
                return "Address";
            default:
                return null;
        }
    }
    
    public Object getValueAt(int row, int column) {
        switch (column) {
            case 0:
                return residentList.get(row).getId();
            case 1:
                return residentList.get(row).getName();
            case 2:
                return residentList.get(row).getPhone();
            case 3:
                return residentList.get(row).getAddress();
            default:
                return null;
        }
    }
    
     public int getColumnCount() {
        return 4;
    }


    public int getRowCount() {
        return residentList.size();
    }
    
    
}
